package llena.catasus.oriol;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.ScreenUtils;

public class TestX implements ApplicationListener, InputProcessor {
	SpriteBatch batch;
	TextureAtlas textureAtlas;
	Animation animation;
	private float elapsedTime = 0;
	int posX;
	int posY;
	int[] speed = new int[2];
	int maxSpeed = 10;
    Sound idle;
    long idleID;
    Sound running;
    long runningID;

    float speedCar = 0, maxSpeedCar = 200, speedIncr = 1;
    float control = 0.5f;

    enum State {
        Acceleration, Desaceleration, Neutral
    }

    State state = State.Neutral;

	@Override
	public boolean keyUp(int keycode) {
		if (keycode == Input.Keys.LEFT || keycode == Input.Keys.RIGHT) {
			speed[0] = 0;
		}
		if (keycode == Input.Keys.UP || keycode == Input.Keys.DOWN) {
			speed[1] = 0;
		}
        if (keycode == Input.Keys.SPACE) {
            state = State.Desaceleration;
        }
		return false;
	}

	@Override
	public boolean keyDown(int keycode) {
		if (keycode == Input.Keys.LEFT) {
			speed[0] = -maxSpeed;
		}
		if (keycode == Input.Keys.RIGHT) {
			speed[0] = maxSpeed;
		}
		if (keycode == Input.Keys.UP) {
			speed[1] = maxSpeed;
		}
		if (keycode == Input.Keys.DOWN) {
			speed[1] = -maxSpeed;
		}
        if (keycode == Input.Keys.SPACE) {
                idle.stop();
                running.stop();
                runningID = running.play(1);
                state = State.Acceleration;
                running.setLooping(runningID, true);
        }
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if (button == 0) {
			posX = screenX - 128;
			posY = Gdx.graphics.getHeight() - screenY - 128;
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	boolean renderPendent = true;
	
	@Override
	public void create () {
		batch = new SpriteBatch();
		Gdx.input.setInputProcessor(this);
		posX = (Gdx.graphics.getWidth()/2) - 128;
		posY = (Gdx.graphics.getHeight()/2) - 128;
		textureAtlas = new TextureAtlas(Gdx.files.internal("TextureAtlas/atlasSprites.atlas"));
		animation = new Animation(1/2.5f, textureAtlas.getRegions());
        idle = Gdx.audio.newSound(Gdx.files.internal("engine-wav/engine-idle.wav"));
        running = Gdx.audio.newSound(Gdx.files.internal("engine-wav/engine-running.wav"));
        idleID = idle.play(1);
        idle.setLooping(idleID, true);
	}

	@Override
	public void resize(int width, int height) {
		Gdx.app.log("resize", "onResize");
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 1, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		//batch.draw(img, 0, 0);
		elapsedTime += Gdx.graphics.getDeltaTime();
		move();
        accelerationCar();
		batch.draw((TextureRegion) animation.getKeyFrame(elapsedTime, true), posX, posY);
		batch.end();
	}

	void move() {
		posX += speed[0];
		posY += speed[1];
	}

    void accelerationCar() {
        if (state == State.Neutral) {
            return;
        }
        if (state == State.Acceleration) {
            if (speedCar < maxSpeedCar) {
                speedCar += speedIncr;
                if (speedCar > maxSpeedCar) {
                    speedCar = maxSpeedCar;
                }
            }
        } else if (state == State.Desaceleration) {
            if (speedCar > 0) {
                speedCar -= speedIncr;
                if (speedCar < 0) {
                    speedCar = 0;
                    state = State.Neutral;
                    running.stop();
                    idle.stop();
                    idleID = idle.play(1);
                    idle.setLooping(idleID, true);
                    idle.setPitch(idleID, 1);
                }
            }
        }
        control = (speedCar/maxSpeedCar)*2 - 0.01f;
        if (control < 0.5f) {
            control = 0.5f;
        } else if (control > 2) {
            control = 2;
        }
        Gdx.app.log("Speed:", ""+speedCar);
        running.setPitch(runningID, control);
    }

	@Override
	public void pause() {
		Gdx.app.log("pause", "onPause");
	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose () {
		batch.dispose();
		//img.dispose();
		textureAtlas.dispose();

        idle.dispose();
        running.dispose();
	}
}
