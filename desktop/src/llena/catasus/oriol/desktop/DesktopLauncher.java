package llena.catasus.oriol.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import llena.catasus.oriol.TestX;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Hello world";
		config.fullscreen = false;
		new LwjglApplication(new TestX(), config);
	}
}
